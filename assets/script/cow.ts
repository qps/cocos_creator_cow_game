/*
 * @Author: Pension 744056574@qq.com
 * @Date: 2023-12-04 11:02:46
 * @LastEditors: Pension 744056574@qq.com
 * @LastEditTime: 2023-12-04 18:15:19
 * @FilePath: /test/assets/script/cow.ts
 * @Description:
 *
 */
import { _decorator, Component, Node, SpriteFrame, Sprite } from "cc";
const { ccclass, property } = _decorator;

@ccclass("cow_skin")
export class cow_skin {
  @property({
    type: [SpriteFrame],
  })
  cow_skin: SpriteFrame[] = [];
}

@ccclass("cow")
export class cow extends Component {
  //   properties: {
  //     cow_skin: {
  //       default: [];
  //       type: [SpriteFrame];
  //     };
  //   };

  @property({
    type: [cow_skin],
  })
  cow_set: cow_skin[] = [];

  intervalTime: number = 0;
  randomType: number = 0;

  start() {
    this.intervalTime = 0;
    this.randomType = 0;
  }

  update(deltaTime: number) {
    this.intervalTime += deltaTime;
    // 每0.2s钟切换一次，牛只有3种皮肤，所以一直 是 0，1，2
    let index = Math.floor(this.intervalTime / 0.2) % 3;

    let cow_set = this.cow_set[this.randomType];

    // 获取真正的牛，给这个真正的牛不断的换皮肤就实现了奔跑的效果
    let sprit = this.node.getComponent(Sprite);
    sprit.spriteFrame = cow_set.cow_skin[index];
  }

  runCallBack() {
    this.randomType = Math.floor(Math.random() * 3);

    // console.log(" this.randomType ", this.randomType);
  }
}
