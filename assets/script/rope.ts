/*
 * @Author: Pension 744056574@qq.com
 * @Date: 2023-12-05 10:02:47
 * @LastEditors: Pension 744056574@qq.com
 * @LastEditTime: 2023-12-05 17:34:23
 * @FilePath: /cocos_creator_test/assets/script/rope.ts
 * @Description:
 *
 */
import { _decorator, Component, Node, tween, v3, SpriteFrame, Sprite, Prefab, instantiate, find, Label, director, assetManager, Texture2D } from "cc";
const { ccclass, property } = _decorator;

@ccclass("rope")
export class rope extends Component {
  @property({
    type: Node,
  })
  rope_node: Node;

  @property({
    type: Node,
  })
  cow_inx: Node;

  @property({
    type: Node,
  })
  bg_sprite: Node;

  @property({
    type: [SpriteFrame],
  })
  ropes: SpriteFrame[] = [];

  @property({
    type: Prefab,
  })
  preFabCow: Prefab;

  @property({
    type: Number,
  })
  time: number = 0;
  score: number = 0;

  successful: boolean = false;
  start() {
    // 倒计时开发
    let countdownLabel = find("Canvas/Sprite/count_down").getComponent(Label);
    countdownLabel.string = "Time:" + this.time + "s";
    this.schedule(() => {
      this.time--;
      if (this.time == 0) {
        let resultNode = find("Canvas/Sprite/result");
        let titleNode = resultNode.getChildByName("title");
        let contentNode = resultNode.getChildByName("content");
        titleNode.getComponent(Label).string = "最终得分：" + this.score;
        let contentLabel = contentNode.getComponent(Label);
        // 展示分数
        switch (true) {
          case this.score < 6:
            contentLabel.string = "菜鸡";
            break;
          case this.score > 10:
            contentLabel.string = "王者";
            break;
          default:
            contentLabel.string = "青铜";
        }

        resultNode.active = true;
        director.pause();
      }

      countdownLabel.string = "Time:" + this.time + "s";
    }, 1);

    // 通过加载远程资源来展示用户头像
    let icon = find("Canvas/Sprite/icon").getComponent(Sprite);
    assetManager.loadRemote("http://r2c.yuouer.com:31555/img/head_img.png", (err, imageAsset) => {
      if (err) {
        return;
      }
      const spriteFrame = new SpriteFrame();
      const texture = new Texture2D();
      texture.image = imageAsset;
      spriteFrame.texture = texture;
      icon.spriteFrame = spriteFrame;
    });
  }

  update(deltaTime: number) {}

  clickCloseBtn() {
    director.resume();
    director.loadScene("main");
  }

  clickCapture(e) {
    this.rope_node.active = true;

    this.rope_node.setPosition(26, -900);
    tween(this.rope_node)
      .to(0.3, { position: v3(26, -257.603, 0) })
      .call(() => {
        // console.log("call");

        let current_x = this.cow_inx.getPosition().x;
        if (current_x > -200 && current_x < 200) {
          // 移除
          let bgNode = this.bg_sprite;
          bgNode.removeChild(this.cow_inx);

          //   获取牛儿的类型
          let roleType = this.cow_inx.getComponent("cow").randomType + 1;
          this.rope_node.getComponent(Sprite).spriteFrame = this.ropes[roleType];

          //   生成新的牛
          this.cow_inx = instantiate(this.preFabCow);
          bgNode.addChild(this.cow_inx);

          this.successful = true;
          this.score++;
        } else {
          console.log("捕捉失败");
        }

        // console.log("current_x", current_x);
      })
      .to(0.3, { position: v3(26, -900, 0) })
      .call(() => {
        this.rope_node.getComponent(Sprite).spriteFrame = this.ropes[0];
        if (this.successful) {
          let scoreLabel = find("Canvas/Sprite/sore").getComponent(Label);
          console.log("scoreLabel", scoreLabel);
          scoreLabel.string = "Score:" + this.score;
          this.successful = false;
        }
      })

      .start();
  }
}
